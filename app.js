const KEY = 'chat';
const SECRET = 'chat';

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();
var expressSession = require('express-session');
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var cookie = cookieParser(SECRET);
var store = new expressSession.MemoryStore();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(cookie);
app.use(expressSession({
  secret: SECRET,
  name: KEY,
  resave: true,
  saveUninitialized: true,
  store: store
}));

// Compartilhando a sessão válida do Express no Socket.IO

io.use(function(socket, next) {
  var data = socket.request;
  cookie(data, {}, function(err) {
    var sessionID = data.signedCookies[KEY];
    store.get(sessionID, function(err, session) {
      if (err || !session) {
        return next(new Error('Acesso negado!'));
      } else {
        socket.handshake.session = session;
        return next();
      }
    });
  });
});


var link = require('./library/conexao');

io.sockets.on('connection', function (client) {
  // Recuperando uma sessão Express.
  var session = client.handshake.session;

  var l = new link(client, {nome: "Fabio", id: "1"})

  client.on('toServer', function (msg) {
    msg = "<b>" + session.nome + ":</b> " + msg + "<br>";
    client.emit('toClient', msg);
    client.broadcast.emit('toClient', msg);
  });
});

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;

server.listen(3000, function(){
  console.log("Rodando o server!");
});


// http://udgwebdev.com/criando-um-chat-usando-session-do-express-4-no-socket-io-1-0
