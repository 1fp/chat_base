var self;

function Conexao(socket, cliente) {
  //contrutor
  self = this;
  self.instance = this.novaConexao(socket, cliente);
}

/* Executa um merge entre as propiedade de dois objetos */
Conexao.prototype.novaConexao = function (obj1, obj2) {
    var obj3 = {};
    for (var attrname in obj1) { obj3[attrname] = obj1[attrname]; }
    for (var attrname in obj2) { obj3[attrname] = obj2[attrname]; }
    return obj3;
};


module.exports = Conexao;
